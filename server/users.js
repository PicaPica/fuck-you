const users=[]; //記錄所有使用者資料{id,name,room}

const addUser = ({id,name,room}) => {
  name=name.trim().toLowerCase(); //去除空格，一律轉成小寫
  room=room.trim().toLowerCase();

  const existingUser=users.find((user)=>user.room===room && user.name===name);
  
  if(existingUser) return { error:'Username is taken' };
 
  

  const user={id,name,room}; //要加入的user
  users.push(user);
  return {user};
  }


const removeUser = (id) =>{

  const index=users.findIndex((user)=>user.id===id)
  if(index!==-1){
    return users.splice(index,1)[0]; //把它存進[0]
  }
}
const getUser= (id) =>{
  return users.find((user)=>user.id===id)
}
const getRoom= (room) =>{
  return users.filter((user)=>user.room===room)
}


module.exports={addUser,removeUser,getUser,getRoom};