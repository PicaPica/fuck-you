import React,{useState} from 'react';
import io from 'socket.io-client';
import queryString from 'querystring';
import { useEffect } from 'react';
import InfoBar from '../InfoBar/infoBar';
import Input from '../Input/input';
import Messages from '../Messages/messages';
import '../Chat/chat.css';

let socket;
const Chat =({location})=>{
    const [name,setName]=useState('');
    const [room,setRoom]=useState('');
    const [message,setMessage]=useState('');  //儲存當前訊息
    const [messages,setMessages]=useState([]); //儲存所有訊息
    const ENDPOINT='localhost:5000';
   useEffect(()=>{
     const {n,name,room}=queryString.parse(location.search);
     
     socket=io(ENDPOINT);
    
     setName(name);
     setRoom(room);

     socket.emit('join',{name,room},(error) => {
      if(error) {
        alert(error);
      }
    });

     return ()=>{
         socket.emit('disconnect');
         socket.off()
     }
   },[ENDPOINT,location.search]); //在ENDPOINT,location.search有變動時才會執行

   useEffect(()=>{
     socket.on('message',(message)=>{
       setMessages([...messages,message]); //將原本的messages複製一份再加上新的message
     });
   },[messages]) //當messages改變時執行此useEffect

   const sendMessage=(event)=>{
     event.preventDefault();
     if (message){
       socket.emit('sendMessage',message,()=>setMessage(''))
     }

   }
   console.log(message,messages);
   return (
     <div className='outerContainer'>
       <div className='container'>
         <InfoBar room={room} />
         <Messages messages={messages} name={name} />
         <Input message={message} setMessage={setMessage} sendMessage={sendMessage}/>
       </div>
     </div>
   )
}
export default Chat;